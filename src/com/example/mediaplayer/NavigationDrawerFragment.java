package com.example.mediaplayer;

import java.util.ArrayList;
import java.util.HashMap;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class NavigationDrawerFragment extends Fragment {
	ActionBarDrawerToggle mDrawerToggle;
	DrawerLayout mDrawerLayout;
	boolean nottolandscape;
	boolean savedinstancestate;
	String key = "prefs";
	View containerview;
	ListView listview;
	TextView songs;
	ListAdapter adapter;
	ArrayList<HashMap<String, String>> listofsongs = new ArrayList<HashMap<String, String>>();
	public static int songIndex;
    Fragtoactivity frag;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.i("test", "cmg to on create");
		super.onCreate(savedInstanceState);
		nottolandscape = Boolean.valueOf(readFromPreferences(getActivity(),
				key, "false"));
		if (savedInstanceState != null) {
			savedinstancestate = true;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreateView(inflater, container, savedInstanceState);
		Log.i("test", "cmg to on create view");
		View v = inflater.inflate(R.layout.nav_drawer, container, false);
		songs = (TextView) v.findViewById(R.id.textView1);
		listview = (ListView) v.findViewById(R.id.listView1);
		MainActivity main = new MainActivity();
		listofsongs = main.getPlayList();
		
		Log.i("test", "the list of songs are" + listofsongs);
		adapter = new ListAdapter(getActivity(), listofsongs);
		listview.setAdapter(adapter);
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
		
				songIndex = position;
			    frag.datapass(songIndex); 
			}
		});

		return v;
	}

	public void setUp(int navdrawer, DrawerLayout drawerlayout, Toolbar toolbar) {
		// TODO Auto-generated method stub
		containerview = getActivity().findViewById(navdrawer);
		mDrawerLayout = drawerlayout;
		mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout,
				toolbar, R.string.Open, R.string.Close) {

			@Override
			public void onDrawerOpened(View drawerView) {
				// TODO Auto-generated method stub
				super.onDrawerOpened(drawerView);
				if (!nottolandscape) {
					nottolandscape = true;
					savepreference(getActivity(), key,
							String.valueOf(nottolandscape));
				}
				getActivity().invalidateOptionsMenu();
			}

			@Override
			public void onDrawerClosed(View drawerView) {

				// TODO Auto-generated method stub
				
				super.onDrawerClosed(drawerView);
				getActivity().invalidateOptionsMenu();
			}

		};

		if (nottolandscape && savedinstancestate == false) {
			mDrawerLayout.openDrawer(containerview);
		}
		mDrawerLayout.setDrawerListener(mDrawerToggle);
		mDrawerLayout.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mDrawerToggle.syncState();
			}
		});
	}

	@SuppressLint("NewApi")
	public static void savepreference(Context context, String preferenceName,
			String perferenceValue) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs",
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(preferenceName, perferenceValue);
		editor.apply();
	}

	public static String readFromPreferences(Context context,
			String preferenceName, String defaultValue) {
		SharedPreferences prefs = context.getSharedPreferences("MyPrefs",
				Context.MODE_PRIVATE);
		return prefs.getString(preferenceName, defaultValue);
	}

	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
		frag=(Fragtoactivity)activity;
	}
}