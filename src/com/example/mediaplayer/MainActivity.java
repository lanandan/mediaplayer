package com.example.mediaplayer;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.RatingBar;
import android.widget.RatingBar.OnRatingBarChangeListener;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("NewApi")
@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class MainActivity extends ActionBarActivity implements Fragtoactivity {
	ImageButton play, forward, backward;
	TextView tv1, tv2;
	MediaPlayer mp;
	RatingBar mRatingbar;
	int seekforward = 5000;
	int seekbackward = 5000;
	SeekBar mSeekbar;
	int getcurrentpos, finaltime,currentSongIndex=0;
	static String path=("/storage/extSdCard/Kiran Songs");

	Handler mHandler = new Handler();
	Toolbar toolbar;
	DrawerLayout mdrwrly;
	NavigationDrawerFragment mFragment;
	static ArrayList<HashMap<String, String>> songsList = new ArrayList<HashMap<String, String>>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		mdrwrly = (DrawerLayout)findViewById(R.id.drawer_layout);
		Log.i("test","the path is"+path);
		toolbar=(Toolbar)findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
	    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	    mFragment=(NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
		mFragment.setUp(R.id.fragment_navigation_drawer,mdrwrly,toolbar);
		mp = new MediaPlayer();
		getcurrentpos = mp.getCurrentPosition();
		
		mRatingbar = (RatingBar) findViewById(R.id.ratingBar1);
		mSeekbar = (SeekBar) findViewById(R.id.seekBar1);
		play = (ImageButton) findViewById(R.id.play);
		forward = (ImageButton) findViewById(R.id.forward);
		backward = (ImageButton) findViewById(R.id.backward);
		tv1 = (TextView) findViewById(R.id.textView1);
		tv2 = (TextView) findViewById(R.id.textView2);
	
		mSeekbar.setOnSeekBarChangeListener(new OnSeekBarChangeListener() {
			
			@Override
			public void onStopTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onStartTrackingTouch(SeekBar seekBar) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void onProgressChanged(SeekBar seekBar, int progress,
					boolean fromUser) {
				// TODO Auto-generated method stub
				if(mp != null && fromUser){
                    mp.seekTo(progress);
                }
			}
		});
		mRatingbar.setOnRatingBarChangeListener(new OnRatingBarChangeListener() {

					@Override
					public void onRatingChanged(RatingBar ratingBar,
							float rating, boolean fromUser) {
						// TODO Auto-generated method stub

						Toast.makeText(getApplicationContext(),
								"Rating is" + rating, Toast.LENGTH_SHORT)
								.show();

					}
				});
           
 		play.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (mp.isPlaying()) {
				
					if (mp != null) 
					{
						mp.pause();
						Toast.makeText(MainActivity.this,
								"Clicked Pause Button", Toast.LENGTH_SHORT)
								.show();
						play.setImageResource(R.drawable.play);
	                     
					} 
					
						}
				else
				{
					if (mp != null) 
				{
					mp.start();
					Toast.makeText(MainActivity.this,
							"Clicked Pause Button", Toast.LENGTH_SHORT)
							.show();
					play.setImageResource(R.drawable.stop);

				} 
					
					}
				

			}
		});
		forward.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int currentposition = mp.getCurrentPosition();
				if (currentposition + seekforward <= mp.getDuration()) {
					mp.seekTo(currentposition + seekforward);
					Toast.makeText(MainActivity.this, "Forwarded 5 Secs",
							Toast.LENGTH_SHORT).show();

				} else {
					mp.seekTo(mp.getDuration());
					Toast.makeText(MainActivity.this, "Song Ended",
							Toast.LENGTH_SHORT).show();

				}
			}
		});
		
		backward.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int currentposition = mp.getCurrentPosition();
				if (currentposition - seekbackward >= 0) {
					mp.seekTo(currentposition - seekbackward);
					Toast.makeText(MainActivity.this, "Backwarded 5 secs",
							Toast.LENGTH_SHORT).show();

				} else {
					mp.seekTo(0);
					Toast.makeText(MainActivity.this, "Song started",
							Toast.LENGTH_SHORT).show();

				}
			}
		});
        
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		ListAdapter.listofsongs.clear();
		mp.stop();
	    finish();  
	}
	
	private void playSong(int currentSongIndex2) throws IllegalArgumentException, SecurityException, IllegalStateException, IOException {
		// TODO Auto-generated method stub
		  mp.reset();
          mp.setDataSource(songsList.get(currentSongIndex2).get("songPath"));
          mp.prepare();
          mp.start(); 
          String songTitle = songsList.get(currentSongIndex2).get("songTitle");
          play.setImageResource(R.drawable.stop);
          mSeekbar.setProgress(0);
          finaltime = mp.getDuration();
         	tv2.setText(String.format("%d min,%d sec",TimeUnit.MILLISECONDS.toMinutes((long) finaltime),TimeUnit.MILLISECONDS.toSeconds((long) finaltime)
									- TimeUnit.MINUTES
											.toSeconds(TimeUnit.MILLISECONDS
													.toMinutes((long) finaltime))));
			
          mHandler.postDelayed(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					getcurrentpos = mp.getCurrentPosition();
 				   tv1.setText(String.format(
 							"%d min, %d sec",TimeUnit.MILLISECONDS.toMinutes((long) getcurrentpos),
 							TimeUnit.MILLISECONDS.toSeconds((long) getcurrentpos)- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
 													.toMinutes((long) getcurrentpos))));
 							
 				    mSeekbar.setProgress(getcurrentpos);
					mSeekbar.setMax(mp.getDuration());

					mHandler.postDelayed(this, 100);
				}
			}, 100);

	}
	
	
	public static ArrayList<HashMap<String, String>> getPlayList(){
        File home = new File(path);
 
        if (home.listFiles(new FileExtensionFilter()).length > 0) {
            for (File file : home.listFiles(new FileExtensionFilter())) {
                HashMap<String, String> song = new HashMap<String, String>();
                song.put("songTitle", file.getName().substring(0, (file.getName().length() - 4)));
                song.put("songPath", file.getPath());
                // Adding each song to SongList
                songsList.add(song);
                
            }
        }
        // return songs list array
        return songsList;
    }

	@Override
	public void datapass(int index) {
		// TODO Auto-generated method stub
		try {
			Log.i("test","the index is "+index);
			playSong(index);
		} catch (IllegalArgumentException | SecurityException
				| IllegalStateException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
}

class FileExtensionFilter implements FilenameFilter{

	@Override
	public boolean accept(File dir, String filename) {
		// TODO Auto-generated method stub
		return (filename.endsWith(".mp3") || filename.endsWith(".MP3"));
	}
	
}