package com.example.mediaplayer;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ListAdapter extends BaseAdapter {
 public static ArrayList<HashMap<String, String>> listofsongs = new ArrayList<HashMap<String, String>>();
	Context context;
	LayoutInflater inflater;
    public static int Position;
	ListAdapter(Context context, ArrayList<HashMap<String, String>> listofsongs) {
		this.context = context;
		this.listofsongs = listofsongs;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listofsongs.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stubs
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		convertView = inflater.inflate(R.layout.single_text, null, false);
		TextView tv1 = (TextView) convertView.findViewById(R.id.textView1);
		tv1.setText(""+listofsongs.get(position));
		return convertView;

    	}

}
